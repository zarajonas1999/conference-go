from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet #Used to encode query sets like n.objects.all()



class DateEncoder(JSONEncoder): #Must write this code before ModelEncoder so it can inherit
    def default(self, o):       #Encodes when the value is a date/time value
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)
        # if o is an instance of datetime
        #    return o.isoformat()
        # otherwise
        #    return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self,o):
        if isinstance(o, QuerySet): #if o is an instance of a queryset, convert into list
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model): # checks if the o object is an instance of self.model (which is a class)
            d = {}
            if hasattr(o, 'get_api_url'): #checks if the instance 'o' has a 'get_api_url' attribute, attributes are either instance or class, instance being parameter and class being methods within the class
                d['href'] = o.get_api_url()
            for key in self.properties:
                value = getattr(o, key) # gets value in the 'o' instance, at the 'key'
                if key in self.encoders:
                    encoder = self.encoders[key]
                    value = encoder.default(value)
                d[key] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self,o):
        return {}
        #   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * if o has the attribute get_api_url,
        #       then add its return value to the dictionary
        #       with the key "href"
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        #   otherwise,
        #       return super().default(o)  # From the documentation
