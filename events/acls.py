from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

#ACLS = Anti Corruption


def get_photo(query):
    url = f'https://api.pexels.com/v1/search?query={query}'
    headers = {
        'Authorization': PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    api_dict = json.loads(response.content) #same as json.loads(response.content), you would have to import json if you choose this way
    return api_dict['photos'][0]['src']['original'] #this returns a URL


def get_weather_data(city,state):
    Geo_URL = f'http://api.openweathermap.org/geo/1.0/direct?q={city} {state}&appid={OPEN_WEATHER_API_KEY}' #allows access to lat and lon
    headers = {
        'Authorization': OPEN_WEATHER_API_KEY
    }
    Geo_response = requests.get(Geo_URL, headers=headers)
    Geo_list = json.loads(Geo_response.content) #same as Geo_response.json(), probably
    lat = Geo_list[0]['lat']
    lon = Geo_list[0]['lon']
    Temp_URL = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial' #allows access to temp
    Temp_response = requests.get(Temp_URL, headers=headers)
    Weather_dict = json.loads(Temp_response.content)
    main_temp = Weather_dict['main']['temp']
    temp_description = Weather_dict['weather'][0]['description']
    if main_temp == None:
        temp_and_description = {'Weather': None}
        return temp_and_description
    temp_and_description = {'Weather': main_temp, 'Description': temp_description}
    return temp_and_description #Returns a dictionary with two properties, or one if null
