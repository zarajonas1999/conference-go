from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
import json
from events.models import State
from .acls import get_photo, get_weather_data

@require_http_methods(['GET', 'POST'])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    conferences = Conference.objects.all()
    if request.method == 'GET':
        return JsonResponse(
                {'conferences': conferences},
                encoder=ConferenceListEncoder
        )
    else:
        content = json.loads(request.body) #content is being assigned to the JSON body in your Insomnia Create POST API
        try:
            location = Location.objects.get(id=content['location']) #Checks if the location object exists
            content['location'] = location #body at 'location' key is equal to location value if it exists
        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location id'},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        'name',
    ]


def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """

    conference = Conference.objects.get(id=id)
    city_boi = conference.location.city
    state_boi = conference.location.state
    weather = get_weather_data(city_boi,state_boi)
    return JsonResponse(
        {"conference": conference, 'weather': weather},
        # conference,
        # {"conference": conference,},
        encoder=ConferenceDetailEncoder,
        safe=False
    )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        'name',
        'city',
    ]
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        'picture_url',
        'weather' #will give Pexel error if weather is not defined
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        'location',
        #'weather',
    ]
    encoders = {
        'location': LocationListEncoder(),
        #'weather': #access to temp encoder here, or not if you use get_extra_data(self,o)
        # 'weather': TempEncoder
    }
    # def get_extra_data(self, o): this will require you to use a Weather model
    #     return {
    #        "weather": o.weather.temp,
    #        'description': o.weather.description
    #     }

# class LocationListEncoder(ModelEncoder):
#     model = Conference
#     properties = ['temp']

@require_http_methods(['GET', 'POST'])
def api_list_locations(request):
    if request.method == 'GET':
        locations = Location.objects.all()
        return JsonResponse(
                locations,
                encoder=LocationListEncoder,
                safe=False
        )
    else:
        content = json.loads(request.body) #JSON dict inputted in Insomnia GET interface
        try:
            state = State.objects.get(abbreviation=content['state'])
            content['state'] = state
            picture_url = get_photo(f"{content['city']} {state.name}") #The space in between converts into something else when used as a URL, in this case; %20
            content['picture_url'] = picture_url
            location = Location.objects.create(**content) #Make sure to place keys you want to create before the .create line of code like this one
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False
            )
        except State.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid state abbreviation'},
                status=400,
            )

    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    # response = []
    # location = Location.objects.all()
    # for l in location:
    #     response.append({
    #         'name': l.name,
    #         'href': l.get_api_url(),
    #     })
    # return JsonResponse({'locations': response})


@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_show_location(request, id):
    if request.method == 'GET':
        location = Location.objects.get(id=id)
        # City = location.city
        # State = location.state
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )
    elif request.method == 'DELETE':
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if 'state' in content:
                state = State.objects.get(abbreviation=content['state'])
                content['state'] = state
        except State.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid state abbreviation'},
                status=400
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )





    # location = Location.objects.get(id=id)
    # return JsonResponse(
    #     location,
    #     encoder=LocationDetailEncoder,
    #     safe=False,
    # )
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }

    location = Location.objects.get(id=id)
    return JsonResponse({
       "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created,
        "updated": location.updated,
        "state": location.state.abbreviation,
    })
    """
